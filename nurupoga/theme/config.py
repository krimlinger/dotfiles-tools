from pathlib import Path
from xdg_base_dirs import xdg_config_home, xdg_data_home

THEME_DIRNAME = "theme"
DEFAULT_THEME = "catppuccin-macchiato"


def get_data_path() -> Path:
    """Return the theme xdg data path."""
    return Path(xdg_data_home()) / THEME_DIRNAME


def get_config_path() -> Path:
    """Return the theme xdg config path."""
    return Path(xdg_config_home()) / THEME_DIRNAME

def get_xdg_config_path() -> Path:
    """Return the system's xdg config path."""
    return Path(xdg_config_home())


def get_config_file_path() -> Path:
    return "current"


def clean_data(s: str) -> str:
    return s.strip()


def load_file(path: Path = get_config_file_path()) -> str:
    with open(get_config_path() / path) as f:
        return clean_data(f.read())


def write_file(data: str, path: Path = get_config_file_path()):
    full_path = get_config_path() / path
    Path(full_path).parent.mkdir(parents=True, exist_ok=True)
    with open(full_path, "w") as f:
        f.write(data)
