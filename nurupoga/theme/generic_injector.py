from pathlib import Path
from dataclasses import asdict
from xdg_base_dirs import xdg_config_home
from . import resources


def inject_generic_theme(template_path: Path,
                         theme_name: str) -> None:
    """Parse a configuration file template and inject a CSS theme into it."""
    config_path = Path(xdg_config_home()) / template_path.with_suffix('')
    # todo: put this in a function ? utils.py module maybe
    Path(config_path).parent.mkdir(parents=True, exist_ok=True)
    colorscheme = resources.load_colorscheme(theme_name)
    template = resources.load_template(template_path)
    with open(config_path, "w") as config_file:
        config_file.write(template.format(**asdict(colorscheme)))
