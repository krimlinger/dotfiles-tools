from dataclasses import dataclass, fields
from pathlib import Path
from colorsys import hls_to_rgb, rgb_to_hls

from .exceptions import ColorSchemeInitializationError


@dataclass
class ColorScheme:
    background: str
    foreground: str
    selection_background: str
    selection_foreground: str
    black: str
    red: str
    green: str
    yellow: str
    blue: str
    magenta: str
    cyan: str
    white: str

    def __str__(self):
        s = ""
        for f in fields(self):
            name = f.name.replace('_', '-')
            s += f"@define-color {name:15} {getattr(self, f.name)};\n"
        return s

    @classmethod
    def from_css_str(cls, s: str):
        # Todo: static types
        def read_until_endcomment(s: str):
            start = s.find('*/')
            if start == -1:
                start = len(s)
                return s[len(s):], True
            return s[start+2:], False
        c = dict()
        in_comment = False
        for line in s.split("\n"):
            line = line.strip()
            if (not in_comment) and (len(line) > 1 and line[0:2] == "/*"):
                line, in_comment = read_until_endcomment(line)
            if in_comment:
                line, in_comment = read_until_endcomment(line)
                if in_comment:
                    continue
            if not line.strip():
                continue
            define, var, value = line.rstrip(";").split(maxsplit=2)
            if define != "@define-color":
                print("Unrecognized css declaration. \
                    Only @define-color is allowed in css colorschemes.")
                continue
            # todo: variable interpolation error
            if value[0] == "@":
                value = c[value[1:]]
            c[var.replace('-', '_')] = value
        try:
            field_names = set(f.name for f in fields(cls))
            return cls(**{k: v for k, v in c.items() if k in field_names})
        except TypeError as e:
            raise ColorSchemeInitializationError from e

    @classmethod
    def from_css_file(cls, colorscheme_path: Path):
        with open(colorscheme_path) as f:
            return ColorScheme.from_css_str(f.read())


def hexstr_to_hls(s: str) -> (int, int, int):
    s = s.strip("#")
    r, g, b = [int(s[i:i+2], 16) for i in range(0, len(s), 2)]
    return rgb_to_hls(r/255., g/255., b/255.)


def hls_to_hexstr(h: int, l: int, s: int) -> str:
    r, g, b = hls_to_rgb(h, l, s)
    return f"#{int(r*255):02x}{int(g*255):02x}{int(b*255):02x}"


def background_lightness(background: str) -> int:
    return hexstr_to_hls(background)[1]


def color_with_background_lightness(hexcolor: str, hexbackground: str) -> str:
    h, l, s = hexstr_to_hls(hexcolor)
    back_l = background_lightness(hexbackground)
    return hls_to_hexstr(h, back_l, s)


def sudo_color(cs: ColorScheme):
    return color_with_background_lightness(cs.red, cs.background)


def ssh_color(cs: ColorScheme):
    return color_with_background_lightness(cs.blue, cs.background)
