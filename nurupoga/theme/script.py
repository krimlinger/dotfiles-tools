from pathlib import Path
from xdg_base_dirs import xdg_data_home, xdg_state_home
import subprocess
import sys
import os
import signal
import click

sys.path.append(str(Path(xdg_data_home()) / "thememanager"))

try:
    from nurupoga.theme import config
    from nurupoga.theme import css_injector
    from nurupoga.theme import generic_injector
    from nurupoga.theme import osc_4_11
    from nurupoga.theme import resources
    from nurupoga.theme.colorscheme import sudo_color, ssh_color
except ImportError as e:
    click.echo("Could not import all required nurupoga.theme modules")
    raise e

WOFI_CSS_CONFIG = Path("wofi/config.css.in")
WAYBAR_CSS_CONFIG = Path("waybar/style.css.in")
CSS_CONFIGS = [WOFI_CSS_CONFIG, WAYBAR_CSS_CONFIG]

MAKO_CONFIG = Path("mako/config.in")
AERC_CONFIG = Path("aerc/stylesets/current.in")
GENERIC_CONFIGS = [MAKO_CONFIG, AERC_CONFIG]


@click.group()
def cli():
    # todo: factor default theme creation with other commands
    try:
        config.load_file()
    except BaseException:
        config.write_file(config.DEFAULT_THEME)


@cli.command()
@click.argument('theme')
@click.pass_context
def set(ctx, theme):
    try:
        cs = resources.load_colorscheme(theme)
    # todo: refine exception handling
    except BaseException as e:
        click.echo("Could not load theme")
        raise RuntimeError() from e
    config.write_file(theme)
    config.write_file(sudo_color(cs), "sudo_color")
    config.write_file(ssh_color(cs), "ssh_color")
    ctx.invoke(update)


@cli.command()
def update():
    theme = config.load_file()
    for conf in CSS_CONFIGS:
        css_injector.inject_css_theme(conf, theme)
    for conf in GENERIC_CONFIGS:
        generic_injector.inject_generic_theme(conf, theme)
    pid_list = Path(xdg_state_home() / "zsh" / "interactive_pid_list")
    try:
        with open(pid_list) as f:
            for pid in f:
                if not pid.strip():
                    continue
                try:
                    os.kill(int(pid), signal.SIGUSR1)
                except ProcessLookupError:
                    # todo: log it ?
                    pass
    except FileNotFoundError:
        # todo: log it ?
        pass
    subprocess.run(["makoctl", "reload"], check=True)
    subprocess.run(["swaymsg", "reload"], check=True)


@cli.command()
def update_shell():
    theme = config.load_file()
    osc_4_11.update_shell_theme(theme)


if __name__ == "__main__":
    cli()
