from pathlib import Path
from importlib import resources
from . import config
from .colorscheme import ColorScheme


def load(resource_type: str, resource_path: Path) -> str:
    """Returns the content of a css colorscheme or template file.
    resource_type is either "css" or "templates". resource_name is the basename
    of a file in any of these directories.
    This function can throw the same exceptions that the open function can.
    """
    try:
        return resources \
            .files(f"nurupoga.{config.THEME_DIRNAME}.{resource_type}") \
            .joinpath(resource_path) \
            .read_text()
    except (FileNotFoundError, ModuleNotFoundError):
        try:
            p = config.get_xdg_config_path() / resource_path
            with open(p) as f:
                return f.read()
        except (FileNotFoundError, ModuleNotFoundError):
            p = config.get_data_path() / resource_type / resource_path
            with open(p) as f:
                return f.read()


def load_colorscheme(theme_path: Path) -> ColorScheme:
    if Path(theme_path).suffix != ".css":
        theme_path += ".css"
    return ColorScheme.from_css_str(load("css", theme_path))


def load_template(template_path: Path) -> str:
    return load("templates", template_path)
