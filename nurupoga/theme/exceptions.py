class ColorSchemeInitializationError(TypeError):
    pass


class InjectionError(RuntimeError):
    pass


class ThemeImportInjectionError(InjectionError):
    pass


class UnkownFieldInjectionError(InjectionError):
    pass
