from pathlib import Path
from xdg_base_dirs import xdg_config_home

from . import resources
from .exceptions import ThemeImportInjectionError


def inject_css_theme(css_template_path: Path,
                     theme_name: str) -> None:
    """Parse a CSS template and try to inject a GTK theme file path into it.

    The first @import statement at the beginning of the template file will
    be searched for "{GTK_THEME_PATH}" which will be replaced by the
    corresponding GTK CSS theme path.

    This function can throw a ThemeImportInjectionError in case of an invalid
    @import statement.
    """
    config_path = Path(xdg_config_home()) / css_template_path.with_suffix('')
    colorscheme = resources.load_colorscheme(theme_name)
    template = resources.load_template(css_template_path)
    with open(config_path, "w") as config_file:
        first_endline = template.find("\n")
        if first_endline == -1:
            raise ThemeImportInjectionError("No first line found")
        if template[0:first_endline].strip() != '@import "{GTK_THEME_PATH}";':
            raise ThemeImportInjectionError(''' @import "{GTK_THEME_PATH}";'''
                                            ''' not found''')
        template = str(colorscheme) + template[first_endline+1:]
        config_file.write(template)
