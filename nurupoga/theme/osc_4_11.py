import click
from pathlib import Path
from . import resources
from .colorscheme import ColorScheme

NUM_TO_NAME = {
    0:  "black",
    1:  "red",
    2:  "green",
    3:  "yellow",
    4:  "blue",
    5:  "magenta",
    6:  "cyan",
    7:  "white",
    8:  "bblack",
    9:  "bred",
    10: "bgreen",
    11: "byellow",
    12: "bblue",
    13: "bmagenta",
    14: "bcyan",
    15: "bwhite",
}


def set_color(c: ColorScheme, color: int):
    name = getattr(c, NUM_TO_NAME[color])
    click.echo(f"\033]4;{color};{name}\033\\", nl=False)


def set_foreground(c: ColorScheme):
    click.echo(f"\033]10;{c.foreground}\033\\", nl=False)


# todo: fix color naming scheme and relation to sway UI elements
def set_background(c: ColorScheme):
    click.echo(f"\033]11;{c.background}\033\\", nl=False)


def update_shell_theme(theme_path: Path):
    c = resources.load_colorscheme(theme_path)
    for d, n in NUM_TO_NAME.items():
        if d > 7:
            continue
        set_color(c, d)
    set_foreground(c)
    set_background(c)
